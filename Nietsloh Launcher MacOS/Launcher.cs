﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using SharpUpdate;
using System.Reflection;
using System.IO;
using MySql.Data.MySqlClient;

namespace Nietsloh_Launcher
{
    public partial class Launcher : Form, ISharpUpdatable
    {

        bool AustelPanelOpen = false;
        bool ErlealysPanelOpen = false;
        bool NietslohPanelOpen = false;
        bool isConnected = false;
        string Username = Login.Username;
        private SharpUpdater updater;
        private MySqlConnection conn;
        private string server;
        private string database;
        private string uid;
        private string password;
        bool FileExist = true;

        bool isShowed = false;

        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenu contextMenu1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem2;

        public static ISharpUpdatable applicationInfo;

        public string ApplicationName
        {
            get { return "Austel"; }
        }

        public string ApplicationID
        {
            get { return "Austel"; }
        }

        public Assembly ApplicationAssembly
        {
            get { return Assembly.GetExecutingAssembly(); }
        }

        public Icon ApplicationIcon
        {
            get { return this.Icon; }
        }

        public Uri UpdateXmlLocation
        {
            get { return new Uri("http://austelclient.net/launcher/update.xml"); }
        }

        public Form Context
        {
            get { return this; }
        }
        

        public Launcher()
        {
            InitializeComponent();

            #region <!> DATABASE <!>
            server = "mysqldb3.ehost-services.com";
            database = "bluexca_erlealys1";
            uid = "bluex_aholstein";
            password = "Firmament0815!";
            #endregion

            string connString;
            connString = $"SERVER={server};DATABASE={database};UID={uid};PASSWORD={password};";

            conn = new MySqlConnection(connString);

            this.components = new System.ComponentModel.Container();
            this.contextMenu1 = new System.Windows.Forms.ContextMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();

            this.contextMenu1.MenuItems.AddRange(
                        new System.Windows.Forms.MenuItem[] { this.menuItem1, this.menuItem2 });

            this.menuItem1.Index = 0;
            this.menuItem1.Text = "Exit";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            this.menuItem2.Index = 1;
            this.menuItem2.Text = "Open";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);

            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);

            notifyIcon1.Icon = new Icon(@"C:\Users\xecra\OneDrive\Documents\Visual Studio 2015\Projects\Nietsloh Launcher\Nietsloh Launcher\favicon (2).ico");

            notifyIcon1.ContextMenu = this.contextMenu1;

            notifyIcon1.Text = "NietslohLauncher";
            notifyIcon1.Visible = true;

            notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);

            #region Transparent Item
            this.bunifuSeparator2.Parent = pictureBox1;
            this.bunifuSeparator3.Parent = pictureBox7;
            this.bunifuSeparator4.Parent = pictureBox10;
            this.bunifuSeparator6.Parent = pictureBox7;
            this.bunifuSeparator1.Parent = pictureBox11;
            this.bunifuSeparator7.Parent = pictureBox11;
            this.bunifuSeparator9.Parent = pictureBox17;
            this.bunifuSeparator8.Parent = pictureBox17;
            this.label3.Parent = pictureBox1;
            this.label4.Parent = pictureBox1;
            this.label17.Parent = pictureBox7;
            this.label13.Parent = pictureBox7;
            this.label14.Parent = pictureBox7;
            this.label15.Parent = pictureBox7;
            this.label16.Parent = pictureBox7;
            this.label19.Parent = pictureBox10;
            this.label20.Parent = pictureBox10;
            this.label21.Parent = pictureBox10;
            this.label22.Parent = pictureBox10;
            this.label28.Parent = pictureBox11;
            this.label26.Parent = pictureBox11;
            this.label11.Parent = pictureBox11;
            this.label27.Parent = pictureBox11;
            this.label25.Parent = pictureBox11;
            this.label11.Parent = pictureBox11;
            this.label27.Parent = pictureBox11;
            this.label25.Parent = pictureBox11;
            this.label29.Parent = pictureBox11;
            this.label12.Parent = pictureBox11;
            this.label30.Parent = pictureBox11;
            this.label31.Parent = pictureBox11;
            this.label24.Parent = pictureBox11;
            this.label23.Parent = pictureBox11;
            this.label33.Parent = pictureBox17;
            this.label43.Parent = pictureBox17;
            this.label40.Parent = pictureBox17;
            this.label39.Parent = pictureBox17;
            this.label38.Parent = pictureBox17;
            this.label37.Parent = pictureBox17;
            this.label40.Parent = pictureBox17;
            this.label34.Parent = pictureBox17;
            this.label35.Parent = pictureBox17;
            this.label42.Parent = pictureBox17;
            this.pictureBox8.Parent = pictureBox11;
            this.pictureBox12.Parent = pictureBox11;
            this.pictureBox13.Parent = pictureBox11;
            this.pictureBox14.Parent = pictureBox17;
            this.pictureBox15.Parent = pictureBox17;
            this.pictureBox16.Parent = pictureBox17;
            this.bunifuFlatButton1.Parent = pictureBox1;
            this.AustelDownload.Parent = pictureBox7;
            this.bunifuFlatButton3.Parent = pictureBox10;
            #endregion

            if(isConnected == false)
            {
                panel5.Visible = false;
            }
            //imageInterval.Start();
            updater = new SharpUpdater(this);
            while (!Username.Contains(""))
            {
                label11.Visible = false;
                label12.Visible = false;
                bunifuSeparator1.Visible = false;
            }
            if (File.Exists(@"Nietsloh Inc\Account\account.txt"))
            {
                /*      label32.Text = getUsername();
                      panel5.Visible = true;*/
                FileExist = false;
                LoginMethod(FileExist);
                this.bunifuMetroTextbox4.Text = getUsername();
                this.bunifuMetroTextbox3.Text = getPassword();
            }
            else
            {
                FileExist = false;
                homePanel.Visible = true;
                austelPanel.Visible = true;
                erlealysPanel.Visible = true;
                registerPanel.Visible = true;
                loginPanel.Visible = true;
            }
            DirectoryInfo dir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + @"\Nietsloh Inc\");
            dir.CreateSubdirectory("Account");
        }
        private void label1_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        public void Signup(string user, string mail, string pass)
        {
            Random r = new Random();
            string query = $"INSERT INTO users(id, username, email, password) VALUES ('" + r.Next(1000, 100000).ToString() + $"', '{user}', '{mail}', '{pass}');";

            MySqlCommand cmd = new MySqlCommand(query, conn);

            conn.Open();
            cmd.ExecuteNonQuery();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label9_Click(object sender, EventArgs e)
        {
            //AUSTELENGINE BUTTON
            if(isConnected == true)
            {
            if (!AustelPanelOpen == true)
            {
                timer1.Start();
            }
            panel4.Visible = false;
            panel3.Visible = true;
            panel5.Visible = false;
            }
        }

        private void label10_Click(object sender, EventArgs e)
        {
            //ERLEALYS BUTTON
            if (isConnected == true)
            {
            if (!ErlealysPanelOpen == true)
            {
                timer2.Start();
            }
            panel3.Visible = false;
            panel4.Visible = true;
            panel5.Visible = false;
            }
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Process.Start("https://github.com/AustelEngine/AustelEngine");
        }

        #region PanelLoader
        private void timer1_Tick(object sender, EventArgs e)
        {
          if(bunifuProgressBar1.Value < 100)
            {
                bunifuProgressBar1.Value += 4;
            }
            else
            {
                homePanel.Visible = true;
                austelPanel.Visible = true;
                erlealysPanel.Visible = false;
                bunifuProgressBar1.Value = 0;
                ErlealysPanelOpen = false;
                AustelPanelOpen = true;
                NietslohPanelOpen = false;
                timer1.Stop();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (bunifuProgressBar1.Value < 100)
            {
                bunifuProgressBar1.Value += 4;
            }
            else
            {
                homePanel.Visible = true;
                austelPanel.Visible = true;
                erlealysPanel.Visible = true;
                bunifuProgressBar1.Value = 0;
                ErlealysPanelOpen = true;
                AustelPanelOpen = false;
                NietslohPanelOpen = false;
                loginPanel.Visible = false;
                registerPanel.Visible = false;
                timer2.Stop();
            }
        }
        private void timer3_Tick(object sender, EventArgs e)
        {
            if (bunifuProgressBar1.Value < 100)
            {
                bunifuProgressBar1.Value += 4;
            }
            else
            {
                homePanel.Visible = true;
                austelPanel.Visible = false;
                erlealysPanel.Visible = false;
                bunifuProgressBar1.Value = 0;
                ErlealysPanelOpen = false;
                AustelPanelOpen = false;
                NietslohPanelOpen = true;
                updater.DoUpdate(this);
                timer3.Stop();
            }
        }
        #endregion

        System.Drawing.Point mMovement = default(System.Drawing.Point);
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mMovement = new Point(-e.X, -e.Y);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            Point mPosition = default(Point);
            if ((e.Button == MouseButtons.Left))
            {
                mPosition = Control.MousePosition;

                mPosition.Offset(mMovement.X, mMovement.Y);
                this.Location = mPosition;
            }
        }

        private void label18_Click(object sender, EventArgs e)
        {
            if (isConnected == true)
            {
            if (!NietslohPanelOpen == true)
            {
                timer3.Start();
            }
                panel3.Visible = false;
                panel4.Visible = false;
                panel5.Visible = true;
            }

        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            if (!AustelPanelOpen == true)
            {
                timer1.Start();
            }
            panel4.Visible = false;
            panel3.Visible = true;
            panel5.Visible = false;
        }

        private void AustelDownload_Click(object sender, EventArgs e)
        {
             AustelDownload.Text = "Waiting...";
              AustelDownload.Normalcolor = Color.DimGray;
              AustelDownload.OnHovercolor = Color.DimGray;
              AustelDownload.Cursor = Cursors.No;
              Thread.Sleep(3000);
              Thread.Sleep(2000);
              AustelDownload.Text = "Download";
              MessageBox.Show("Error, please try later or contact us on our website.", "Error");
              AustelDownload.Normalcolor = Color.Gray;
              AustelDownload.OnHovercolor = Color.DarkGray;
              AustelDownload.Cursor = Cursors.Hand;
          /*  Process process = Process.Start(@"C:\Users\xecra\OneDrive\Documents\Visual Studio 2015\Projects\AustelEngine\AustelEngine\bin\Debug\AustelEngine.exe");
            int id = process.Id;
            Process tempProc = Process.GetProcessById(id);
            this.Visible = false;
            tempProc.WaitForExit();
            this.Visible = true;*/
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            isShowed = true;
            timer4.Stop();
        }

        private void label12_Click(object sender, EventArgs e)
        {
            Register register = new Nietsloh_Launcher.Register();
            register.Show();
        }

        private void label11_Click(object sender, EventArgs e)
        {
            Login login = new Nietsloh_Launcher.Login();
            login.Show();
        }

        private void imageInterval_Tick(object sender, EventArgs e)
        {
        }

        private void Launcher_Load(object sender, EventArgs e)
        {
           // updater.DoUpdate(this);
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Soon !", "Error");
        }

        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {
            bunifuFlatButton3.Text = "Waiting...";
            bunifuFlatButton3.Normalcolor = Color.DimGray;
            bunifuFlatButton3.OnHovercolor = Color.DimGray;
            bunifuFlatButton3.Cursor = Cursors.No;
            Thread.Sleep(3000);
            Thread.Sleep(2000);
            bunifuFlatButton3.Text = "Download";
            MessageBox.Show("Error, please try later or contact us on our website.", "Error");
            bunifuFlatButton3.Normalcolor = Color.Gray;
            bunifuFlatButton3.OnHovercolor = Color.DarkGray;
            bunifuFlatButton3.Cursor = Cursors.Hand;
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            string UsernameField = bunifuMetroTextbox1.Text;
            string MailField = PasswordField.Text;
            string PassField = bunifuMetroTextbox2.Text;

            if (MailField.Contains("@gmail") || MailField.Contains("@icloud") || MailField.Contains("@yahoo") || MailField.Contains("@outlook") || MailField.Contains("@icloud"))
            {
                Signup(UsernameField, MailField, PassField);
                Thread.Sleep(1500);
                isConnected = true;
                label32.Text = UsernameField;
                label32.Cursor = Cursors.Hand;
                panel3.Visible = false;
                panel4.Visible = false;
                panel5.Visible = true;

                if (!NietslohPanelOpen == true)
                {
                    timer3.Start();
                }
            }
            else
            {
                MessageBox.Show("Error ! Check our registration rules.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label31_Click(object sender, EventArgs e)
        {
            homePanel.Visible = true;
            austelPanel.Visible = true;
            erlealysPanel.Visible = true;
            registerPanel.Visible = true;
            loginPanel.Visible = true;
        }


        static string GetLine(string fileName, int line)
        {
            using (StreamReader sr = new StreamReader(fileName))
            {
                for (int i = 1; i < line; i++)
                    sr.ReadLine();
                return sr.ReadLine();
            }
        }

        public static string getUsername()
        {
            string file = GetLine(@"Nietsloh Inc\Account\account.txt", 1);
            return file;
        }

        public static string getPassword()
        {
            string file = GetLine(@"Nietsloh Inc\Account\account.txt", 2);
            return file;
        }

        public int money = 0;

        private void bunifuFlatButton2_Click_1(object sender, EventArgs e)
        {
            LoginMethod(FileExist);
        }

        void LoginMethod(bool FileExist)
        {
            if (FileExist == false)
            {
                this.FileExist = FileExist;
                conn.Open();
                string user = bunifuMetroTextbox4.Text;
                string pass = bunifuMetroTextbox3.Text;
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM users WHERE username='" + user + "' AND password ='" + pass + "' ", conn);

                MySqlDataReader leer = cmd.ExecuteReader();
                if (leer.Read())
                {
                    this.Refresh();
                    this.Update();
                    isConnected = true;
                    pictureBox18.Visible = true;
                    label32.Text = user;
                    this.money = (int)leer["money"];
                    label36.Text = "$" + money.ToString() + " N'Coins";
                    if (isConnected == true)
                    {
                        if (!NietslohPanelOpen == true)
                        {
                            timer3.Start();
                        }
                        panel3.Visible = false;
                        panel4.Visible = false;
                        panel5.Visible = true;
                    }
                }
            }
        }

        private void label35_Click(object sender, EventArgs e)
        {
            homePanel.Visible = true;
            austelPanel.Visible = true;
            erlealysPanel.Visible = true;
            registerPanel.Visible = true;
            loginPanel.Visible = false;
        }

        private void label32_MouseEnter(object sender, EventArgs e)
        {

            Font font = new Font(label32.Font.Name, label32.Font.SizeInPoints, FontStyle.Underline);
            label32.Font = font;
        }

        private void label32_MouseLeave(object sender, EventArgs e)
        {
            Font font = new Font(label32.Font.Name, label32.Font.SizeInPoints, FontStyle.Regular);
            label32.Font = font;
        }

        #region click
        private void label43_Click(object sender, EventArgs e)
        {

        }

        private void label36_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox18_Click(object sender, EventArgs e)
        {

        }

        private void label32_Click(object sender, EventArgs e)
        {

        }

        private void bunifuSeparator5_Load(object sender, EventArgs e)
        {

        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bunifuProgressBar1_progressChanged(object sender, EventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void erlealysPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void registerPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void loginPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox17_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox14_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Soon !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void pictureBox15_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Soon !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void pictureBox16_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Soon !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        private void label34_Click(object sender, EventArgs e)
        {

        }

        private void label37_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox3_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label38_Click(object sender, EventArgs e)
        {

        }

        private void label39_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox4_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label40_Click(object sender, EventArgs e)
        {

        }

        private void bunifuSeparator8_Load(object sender, EventArgs e)
        {

        }

        private void bunifuSeparator9_Load(object sender, EventArgs e)
        {

        }

        private void label42_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox13_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {

        }

        private void label30_Click(object sender, EventArgs e)
        {

        }

        private void label33_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click_1(object sender, EventArgs e)
        {

        }

        private void label25_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox2_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label26_Click(object sender, EventArgs e)
        {

        }

        private void label27_Click(object sender, EventArgs e)
        {

        }

        private void PasswordField_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label28_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox1_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void label29_Click(object sender, EventArgs e)
        {

        }

        private void bunifuSeparator7_Load(object sender, EventArgs e)
        {

        }

        private void bunifuSeparator1_Load(object sender, EventArgs e)
        {

        }

        private void label12_Click_1(object sender, EventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void bunifuSeparator4_Load(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {

        }

        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void homePanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void austelPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bunifuSeparator6_Load(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            Process.Start("https://github.com/AustelEngine/AustelEngine");
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Soon !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void bunifuSeparator3_Load(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {

        }

        private void bunifuSeparator2_Load(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            //PATCH NOTE NIETSLOH
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
        #endregion

        private void notifyIcon1_DoubleClick(object Sender, EventArgs e)
        {
            this.Visible = true;
        }

        private void menuItem1_Click(object Sender, EventArgs e)
        {
            Application.Exit();
        }
        private void menuItem2_Click(object Sender, EventArgs e)
        {
            this.Visible = true;
        }
    }
}
  