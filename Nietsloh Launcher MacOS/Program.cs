﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nietsloh_Launcher
{
    class Program
    {

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            string ProcName = AppDomain.CurrentDomain.FriendlyName.Replace(".exe", "");

            int r = 0;

            foreach (Process p in Process.GetProcesses())
            {
                if (p.ProcessName == ProcName)
                {
                    r = r + 1;
                }

            }
            if (r > 1)
            {
                MessageBox.Show("The application is already running !", "Error !", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
            if (!IsConnectedToInternet() == true)
            {
                Application.Run(new NoInternetConnection());
            }
            else
            {
                Application.Run(new SplashScreen());
            }
            }
        }


        public static bool IsConnectedToInternet()
        {
            try
            {
                using (var client = new WebClient())
                {
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

    }
}
