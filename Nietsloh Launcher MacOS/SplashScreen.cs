﻿using SharpUpdate;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nietsloh_Launcher
{
    public partial class SplashScreen : Form, ISharpUpdatable
    {

        public static ISharpUpdatable applicationInfo;
        private SharpUpdater updater;

        private Launcher launcher = new Launcher();

        public string ApplicationName
        {
            get { return "Austel"; }
        }

        public string ApplicationID
        {
            get { return "Austel"; }
        }

        public Assembly ApplicationAssembly
        {
            get { return Assembly.GetExecutingAssembly(); }
        }

        public Icon ApplicationIcon
        {
            get { return this.Icon; }
        }

        public Uri UpdateXmlLocation
        {
            get { return new Uri("http://austelclient.net/launcher/update.xml"); }
        }

        public Form Context
        {
            get { return this; }
        }

        public SplashScreen()
        {
            InitializeComponent();
            updater = new SharpUpdater(this);


            /*         Thread.Sleep(3000);
                     label4.Text = "Verifing update..";
                     Thread.Sleep(3000);
                     if (updater.UpdateIsAvailable == true)
                     {
                         updater.DoUpdate();
                         this.Hide();
                     }
                     else
                     {
                         label4.Text = "No update available";
                     }
                    Thread.Sleep(800);
                     label4.Text = "Launch Launcher..";*/
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Application.Exit(); 
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (progressBar.Value < 100)
            {
                progressBar.Value++;
                updater.DoUpdate(this);
                string myDocumentsPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory);
                if (!Directory.Exists(myDocumentsPath + @"\Nietsloh Inc"))
                {
                    Directory.CreateDirectory(myDocumentsPath + @"\Nietsloh Inc");
                }
            }
            else if (progressBar.Value == 100)
            {
                Launcher form = new Launcher();
                form.Show();
                this.Hide();
                timer1.Stop();
            }
        }

        private void SplashScreen_Load(object sender, EventArgs e)
        {
            timer1.Start();
            label4.Text = "Loading...";
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            label4.Visible = false;
            label5.Visible = true;
            timer2.Stop();
            timer3.Start();
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            label5.Text = "Initializing Windows..";
            timer3.Stop();
        }
    }
}
